import React from 'react';
import logo from './logo.svg';
import './App.css';
import DemoProps from './DemoProps/DemoProps';
import TodoList from './Ex_TodoList/TodoList/TodoList';
import Ex_TodoList from './Ex_TodoList/Ex_TodoList';

function App() {
  return (
    <div className='App'>
      {/* <DemoProps /> */}
      <Ex_TodoList />
    </div>
  );
}

export default App;
