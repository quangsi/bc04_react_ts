export interface InterfaceTodo {
  id: string;
  text: string;
  isCompleted: boolean;
}

export interface InterTodoListComponent {
  todos: InterfaceTodo[];
}

export interface InterfaceTodoItemComponent {
  todo: InterfaceTodo;
  // handleRemove
}
