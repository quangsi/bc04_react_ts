import React, { useState } from 'react';
import { InterfaceTodo } from './interface/Interface_Ex_TodoList';
import TodoForm from './TodoForm/TodoForm';
import TodoList from './TodoList/TodoList';

export default function Ex_TodoList() {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    {
      id: '1',
      text: 'Làm dự án cuối khoá',
      isCompleted: false,
    },
    {
      id: '2',
      text: 'Làm capstone React',
      isCompleted: false,
    },
  ]);
  return (
    <div>
      <TodoForm />
      <TodoList todos={todos} />
    </div>
  );
}
