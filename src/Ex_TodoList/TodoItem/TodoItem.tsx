import React, { useEffect } from 'react';
import { InterfaceTodoItemComponent } from '../interface/Interface_Ex_TodoList';

export default function TodoItem({ todo }: InterfaceTodoItemComponent) {
  useEffect(() => {}, []);
  if (2 > 1) {
  }
  return (
    <tr className='bg-white border-b dark:bg-gray-800 dark:border-gray-700'>
      <th
        scope='row'
        className='py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white'>
        {todo.id}
      </th>
      <td className='py-4 px-6'>{todo.text}</td>
      <td className='py-4 px-6'>
        <input type='checkbox' checked={todo.isCompleted} />
      </td>
    </tr>
  );
}
